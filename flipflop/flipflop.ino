void setup () {
  //set d2 sebagai OUTPUT
  pinMode(2, OUTPUT);

}
void loop () {
  //tulis HIGH ke d2
  digitalWrite(2, HIGH);
  //delay selama 3 detik
  delay(3000);
  //tulis LOW ke d2
  digitalWrite(2, LOW);
  //delay selama 3 detik
  delay(3000);
}
